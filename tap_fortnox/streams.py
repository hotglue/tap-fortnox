"""Stream type classes for tap-fortnox."""

from pathlib import Path
from typing import Any, Dict, Iterable, List, Optional, Union

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_fortnox.client import FortnoxStream


class ArticlesStream(FortnoxStream):
    """Define custom stream."""

    name = "articles"
    path = "/3/articles"
    primary_keys = ["ArticleNumber"]
    replication_key = None
    records_jsonpath = "$.Articles[*]"

    schema = th.PropertiesList(
        th.Property("@url", th.StringType),
        th.Property("ArticleNumber", th.StringType),
        th.Property("Description", th.StringType),
        th.Property("DisposableQuantity", th.CustomType({"type": ["number", "string"]})),
        th.Property("EAN", th.StringType),
        th.Property("Housework", th.BooleanType),
        th.Property("PurchasePrice", th.CustomType({"type": ["number", "string"]})),
        th.Property("SalesPrice", th.CustomType({"type": ["number", "string"]})),
        th.Property("QuantityInStock", th.CustomType({"type": ["number", "string"]})),
        th.Property("ReservedQuantity", th.CustomType({"type": ["number", "string"]})),
        th.Property("StockPlace", th.StringType),
        th.Property("StockValue", th.CustomType({"type": ["number", "string"]})),
        th.Property("Unit", th.StringType),
        th.Property("VAT", th.CustomType({"type": ["number", "string"]})),
        th.Property("WebshopArticle", th.BooleanType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:

        return {
            "article_number": record["ArticleNumber"],
        }


class ArticleDetailsStream(FortnoxStream):
    """Define custom stream."""

    name = "article_details"
    path = "/3/articles/{article_number}"
    primary_keys = ["ArticleNumber"]
    replication_key = None
    records_jsonpath = "$.Article[*]"
    parent_stream_type = ArticlesStream

    schema = th.PropertiesList(
        th.Property("@url", th.StringType),
        th.Property("ArticleNumber", th.StringType),
        th.Property("Bulky", th.BooleanType),
        th.Property("ConstructionAccount", th.CustomType({"type": ["number", "string"]})),
        th.Property("Depth", th.CustomType({"type": ["number", "string"]})),
        th.Property("Description", th.StringType),
        th.Property("DisposableQuantity", th.CustomType({"type": ["number", "string"]})),
        th.Property("EAN", th.StringType),
        th.Property("EUAccount", th.CustomType({"type": ["number", "string"]})),
        th.Property("EUVATAccount", th.CustomType({"type": ["number", "string"]})),
        th.Property("ExportAccount", th.CustomType({"type": ["number", "string"]})),
        th.Property("Height", th.CustomType({"type": ["number", "string"]})),
        th.Property("Housework", th.BooleanType),
        th.Property("HouseworkType", th.StringType),
        th.Property("Active", th.BooleanType),
        th.Property("Manufacturer", th.StringType),
        th.Property("ManufacturerArticleNumber", th.StringType),
        th.Property("Note", th.StringType),
        th.Property("PurchaseAccount", th.CustomType({"type": ["number", "string"]})),
        th.Property("PurchasePrice", th.CustomType({"type": ["number", "string"]})),
        th.Property("QuantityInStock", th.CustomType({"type": ["number", "string"]})),
        th.Property("ReservedQuantity", th.CustomType({"type": ["number", "string"]})),
        th.Property("SalesAccount", th.CustomType({"type": ["number", "string"]})),
        th.Property("StockGoods", th.BooleanType),
        th.Property("StockPlace", th.StringType),
        th.Property("StockValue", th.CustomType({"type": ["number", "string"]})),
        th.Property("StockWarning", th.CustomType({"type": ["number", "string"]})),
        th.Property("SupplierName", th.StringType),
        th.Property("SupplierNumber", th.StringType),
        th.Property("Type", th.StringType),
        th.Property("Unit", th.StringType),
        th.Property("VAT", th.CustomType({"type": ["number", "string"]})),
        th.Property("WebshopArticle", th.BooleanType),
        th.Property("Weight", th.CustomType({"type": ["number", "string"]})),
        th.Property("Width", th.CustomType({"type": ["number", "string"]})),
        th.Property("Expired", th.BooleanType),
        th.Property("SalesPrice", th.CustomType({"type": ["number", "string"]})),
        th.Property("CostCalculationMethod", th.StringType),
        th.Property("StockAccount", th.CustomType({"type": ["number", "string"]})),
        th.Property("StockChangeAccount", th.CustomType({"type": ["number", "string"]})),
        th.Property("DirectCost", th.CustomType({"type": ["number", "string"]})),
        th.Property("FreightCost", th.CustomType({"type": ["number", "string"]})),
        th.Property("OtherCost", th.CustomType({"type": ["number", "string"]})),
        th.Property("DefaultStockPoint", th.StringType),
        th.Property("DefaultStockLocation", th.StringType),
    ).to_dict()


class CurrenciesStream(FortnoxStream):
    """Define custom stream."""

    name = "currencies"
    path = "/3/currencies"
    primary_keys = ["currency"]
    replication_key = None

    schema = th.PropertiesList(
        th.Property("currency", th.StringType),
        th.Property("rate", th.CustomType({"type": ["number", "string"]})),
        th.Property("unit", th.CustomType({"type": ["number", "string"]})),
    ).to_dict()


class SupplierStream(FortnoxStream):
    """Define custom stream."""

    name = "suppliers"
    path = "/3/suppliers"
    primary_keys = ["SupplierNumber"]
    records_jsonpath = "$.Suppliers[*]"
    replication_key = None

    schema = th.PropertiesList(
        th.Property("@url", th.StringType),
        th.Property("Active", th.BooleanType),
        th.Property("Address1", th.StringType),
        th.Property("Address2", th.StringType),
        th.Property("Bank", th.StringType),
        th.Property("BankAccountNumber", th.StringType),
        th.Property("BG", th.StringType),
        th.Property("BIC", th.StringType),
        th.Property("BranchCode", th.StringType),
        th.Property("City", th.StringType),
        th.Property("CostCenter", th.StringType),
        th.Property("CountryCode", th.StringType),
        th.Property("Currency", th.StringType),
        th.Property("DisablePaymentFile", th.BooleanType),
        th.Property("Email", th.StringType),
        th.Property("IBAN", th.StringType),
        th.Property("Name", th.StringType),
        th.Property("OrganisationNumber", th.StringType),
        th.Property("PG", th.StringType),
        th.Property("Phone1", th.StringType),
        th.Property("PreDefinedAccount", th.StringType),
        th.Property("Project", th.StringType),
        th.Property("SupplierNumber", th.StringType),
        th.Property("TermsOfPayment", th.StringType),
        th.Property("ZipCode", th.StringType),
    ).to_dict()


class PurchaseOrdersStream(FortnoxStream):
    """Define custom stream."""

    name = "purchase_orders"
    path = "/api/warehouse/purchaseorders-v1"
    primary_keys = ["id"]
    replication_key = None

    schema = th.PropertiesList(
        th.Property("deliveryDate", th.DateType),
        th.Property("note", th.StringType),
        th.Property("internalReference", th.StringType),
        th.Property("supplierName", th.StringType),
        th.Property("messageToSupplier", th.StringType),
        th.Property("id", th.CustomType({"type": ["number", "string"]})),
        th.Property("orderDate", th.DateType),
        th.Property("supplierNumber", th.StringType),
        th.Property("rows", th.CustomType({"type": ["array", "string"]})),
        th.Property("stockPointId", th.StringType),
        th.Property("deliveryName", th.StringType),
        th.Property("deliveryAddress", th.StringType),
        th.Property("deliveryAddress2", th.StringType),
        th.Property("deliveryZipCode", th.StringType),
        th.Property("deliveryCity", th.StringType),
        th.Property("deliveryCountryCode", th.StringType),
        th.Property("supplierAddress", th.StringType),
        th.Property("supplierAddress2", th.StringType),
        th.Property("supplierPostCode", th.StringType),
        th.Property("supplierCity", th.StringType),
        th.Property("supplierCountryCode", th.StringType),
        th.Property("paymentTermsCode", th.StringType),
        th.Property("languageCode", th.StringType),
        th.Property("currencyCode", th.StringType),
        th.Property("ourReference", th.StringType),
        th.Property("yourReference", th.StringType),
        th.Property("confirmationEmail", th.StringType),
        th.Property("projectId", th.StringType),
        th.Property("costCenterCode", th.StringType),
        th.Property("stockPointCode", th.StringType),
        th.Property("totalReceivedQuantity", th.CustomType({"type": ["number", "string"]})),
        th.Property("voided", th.BooleanType),
        th.Property("responseState", th.StringType),
        th.Property("purchaseOrderState", th.StringType),
        th.Property("supplierEmail", th.StringType),
        th.Property("currencyRate", th.CustomType({"type": ["number", "string"]})),
        th.Property("currencyUnit", th.CustomType({"type": ["number", "string"]})),
        th.Property("orderValue", th.CustomType({"type": ["number", "string"]})),
        th.Property("manuallyCompleted", th.BooleanType),
        th.Property("purchaseType", th.StringType),
        th.Property("customerId", th.StringType),
        th.Property("customerNumber", th.StringType),
        th.Property("customerName", th.StringType),
        th.Property("orderValueInSEK", th.CustomType({"type": ["number", "string"]})),
        th.Property("supplier", th.StringType),
        th.Property("translatedResponseState", th.StringType),
        th.Property(
            "outboundDocumentReference", th.CustomType({"type": ["object", "string"]})
        ),
    ).to_dict()


class OrdersStream(FortnoxStream):
    """Define custom stream."""

    name = "orders"
    path = "/3/orders/"
    primary_keys = ["DocumentNumber"]
    replication_key = None
    records_jsonpath = "$.Orders[*]"
    schema = th.PropertiesList(
        th.Property("deliveryDate", th.DateType),
        th.Property("@url", th.StringType),
        th.Property("Cancelled", th.BooleanType),
        th.Property("Currency", th.StringType),
        th.Property("CustomerName", th.StringType),
        th.Property("CustomerNumber", th.StringType),
        th.Property("DeliveryDate", th.DateType),
        th.Property("DocumentNumber", th.StringType),
        th.Property("ExternalInvoiceReference1", th.StringType),
        th.Property("ExternalInvoiceReference2", th.StringType),
        th.Property("OrderDate", th.DateType),
        th.Property("OrderType", th.StringType),
        th.Property("Project", th.StringType),
        th.Property("Sent", th.BooleanType),
        th.Property("Total", th.CustomType({"type": ["number", "string"]})),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:

        return {
            "document_number": record["DocumentNumber"],
        }


class OrderDetailsStream(FortnoxStream):
    """Define custom stream."""

    name = "order_details"
    path = "/3/orders/{document_number}"
    primary_keys = ["DocumentNumber"]
    replication_key = None
    records_jsonpath = "$.Order[*]"
    parent_stream_type = OrdersStream
    schema = th.PropertiesList(
        th.Property("@url", th.StringType),
        th.Property("@urlTaxReductionList", th.StringType),
        th.Property("AdministrationFee", th.CustomType({"type": ["number", "string"]})),
        th.Property("AdministrationFeeVAT", th.CustomType({"type": ["number", "string"]})),
        th.Property("Address1", th.StringType),
        th.Property("Address2", th.StringType),
        th.Property("BasisTaxReduction", th.CustomType({"type": ["number", "string"]})),
        th.Property("Cancelled", th.BooleanType),
        th.Property("City", th.StringType),
        th.Property("Comments", th.StringType),
        th.Property("ContributionPercent", th.CustomType({"type": ["number", "string"]})),
        th.Property("ContributionValue", th.CustomType({"type": ["number", "string"]})),
        th.Property("CopyRemarks", th.BooleanType),
        th.Property("Country", th.StringType),
        th.Property("CostCenter", th.StringType),
        th.Property("Currency", th.StringType),
        th.Property("CurrencyRate", th.CustomType({"type": ["number", "string"]})),
        th.Property("CurrencyUnit", th.CustomType({"type": ["number", "string"]})),
        th.Property("CustomerName", th.StringType),
        th.Property("CustomerNumber", th.StringType),
        th.Property("DeliveryState", th.StringType),
        th.Property("DeliveryAddress1", th.StringType),
        th.Property("DeliveryAddress2", th.StringType),
        th.Property("DeliveryCity", th.StringType),
        th.Property("DeliveryCountry", th.StringType),
        th.Property("deliveryDate", th.DateType),
        th.Property("DeliveryName", th.StringType),
        th.Property("DeliveryZipCode", th.StringType),
        th.Property("DocumentNumber", th.StringType),
        th.Property("EmailInformation", th.CustomType({"type": ["object", "string"]})),
        th.Property("ExternalInvoiceReference1", th.StringType),
        th.Property("ExternalInvoiceReference2", th.StringType),
        th.Property("Freight", th.CustomType({"type": ["number", "string"]})),
        th.Property("FreightVAT", th.CustomType({"type": ["number", "string"]})),
        th.Property("Gross", th.CustomType({"type": ["number", "string"]})),
        th.Property("HouseWork", th.BooleanType),
        th.Property("InvoiceReference", th.StringType),
        th.Property("Labels", th.CustomType({"type": ["array", "string"]})),
        th.Property("Language", th.StringType),
        th.Property("Net", th.CustomType({"type": ["number", "string"]})),
        th.Property("NotCompleted", th.BooleanType),
        th.Property("OfferReference", th.StringType),
        th.Property("OrderDate", th.DateType),
        th.Property("OrderRows", th.CustomType({"type": ["array", "string"]})),
        th.Property("OrderType", th.StringType),
        th.Property("OrganisationNumber", th.StringType),
        th.Property("OurReference", th.StringType),
        th.Property("Phone1", th.StringType),
        th.Property("Phone2", th.StringType),
        th.Property("PriceList", th.StringType),
        th.Property("PrintTemplate", th.StringType),
        th.Property("Project", th.StringType),
        th.Property("WarehouseReady", th.BooleanType),
        th.Property("OutboundDate", th.DateType),
        th.Property("Remarks", th.StringType),
        th.Property("RoundOff", th.CustomType({"type": ["number", "string"]})),
        th.Property("Sent", th.BooleanType),
        th.Property("TaxReduction", th.CustomType({"type": ["number", "string"]})),
        th.Property("TermsOfDelivery", th.StringType),
        th.Property("TermsOfPayment", th.StringType),
        th.Property("TimeBasisReference", th.CustomType({"type": ["number", "string"]})),
        th.Property("Total", th.CustomType({"type": ["number", "string"]})),
        th.Property("TotalToPay", th.CustomType({"type": ["number", "string"]})),
        th.Property("TotalVAT", th.CustomType({"type": ["number", "string"]})),
        th.Property("VATIncluded", th.BooleanType),
        th.Property("WayOfDelivery", th.StringType),
        th.Property("YourReference", th.StringType),
        th.Property("YourOrderNumber", th.StringType),
        th.Property("ZipCode", th.StringType),
        th.Property("StockPointCode", th.StringType),
        th.Property("StockPointId", th.StringType),
        th.Property("TaxReductionType", th.StringType),
    ).to_dict()

class StockPointsStream(FortnoxStream):
    """Define custom stream."""

    name = "stock_points"
    path = "/api/warehouse/stockpoints-v1"
    primary_keys = ["id"]
    replication_key = None

    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("code", th.StringType),
        th.Property("name", th.StringType),
        th.Property("stockLocations", th.ArrayType(
            th.ObjectType(
                th.Property("id", th.StringType),
                th.Property("code", th.StringType),
                th.Property("name", th.StringType),
                th.Property("stockPointId", th.StringType),
            )
        )),
        th.Property("usingCompanyAddress", th.BooleanType),
        th.Property("deliveryName", th.StringType),
        th.Property("deliveryAddress", th.StringType),
        th.Property("deliveryAddress2", th.StringType),
        th.Property("deliveryZipCode", th.StringType),
        th.Property("deliveryCity", th.StringType),
        th.Property("deliveryPhone", th.StringType),
        th.Property("deliveryCountryCode", th.StringType),
        th.Property("active", th.BooleanType),
    ).to_dict()

class StockStatusStream(FortnoxStream):
    """Define custom stream."""

    name = "stock_status"
    path = "/api/warehouse/status-v1/stockbalance" 
    primary_keys = ["id"]
    replication_key = None

    schema = th.PropertiesList(
        th.Property("itemId", th.StringType),
        th.Property("availableStock", th.NumberType),
        th.Property("inStock", th.NumberType),
        th.Property("stockPointCode", th.StringType),
    ).to_dict()
